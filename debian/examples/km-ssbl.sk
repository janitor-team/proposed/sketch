%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% Super short baseline (SSBL) illustration
%
% Author: Kjell Magne Fauske
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Copyright (c) 2009 Kjell Magne Fauske
% This code is released under the MIT license
% See http://www.opensource.org/licenses/mit-license.php for details. 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Define some basic constants
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

def O (0,0,0) % Origo

% The ee constant is used for tweaking the hidden surface removal algorithm
% in Sketch. The constant is too small to give visible results, but affects
% which surfaces that are drawn. 
def ee 0.0001

% 
% Draw the axes of the current coordinate system
%
def sz 4 % length of each axis
def axes {
    line [arrows=<->,draw=blue,line width=0.5pt] (sz,0,0)(O)(0,sz,0)
    line [arrows=->,draw=blue,line width=.5pt]  (O)(0,0,sz)
    special |\node[above] at #1 {\scriptsize $z$};|(0,0,sz)
    special |\node[above] at #1 {\scriptsize $x$};|(0,sz,0)
    special |\node[above] at #1 {\scriptsize $y$};|(sz,0,0)
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Define some basic geometric shapes like cubes and cylinders. 
% All the shapes have unit sizes and are ment to be scaled when used.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

def jR 1
def jR_inner 0.5/2
def jH 2/2

def cylinder put{rotate(90,[1,0,0])}{
    def n 10   
  sweep[draw=gray, line width= 0.01pt]{n<>, rotate(360/n,[0,1,0])}
    line[draw=black,fill=lightgray](jR,0)(jR,jH)
}

% A unit cube with center (0,0,0)
def box put{translate([-1.5,-0.5,0])}{
    def n 1   
  sweep[fill=lightgray,  draw=gray]
        {n,translate([0,n,0])}
    sweep[fill=lightgray,  draw=gray]
        {n, translate([n,0,0])}line[fillcolor=gray](1,0,0)(1,0,1)
}

def sphere put{rotate(90,[0,1,0])}{
   def n_faces 12
   sweep[fill=red, line width= 0.1pt] { n_faces, rotate(360/n_faces, [1,0,0])}
       sweep {n_faces, rotate(360/n_faces)}(0,1)
}


% draw a simple beacon
def beacon {
    put {scale(0.1)}{sphere}
    put {translate([0,0,-1]) then scale([0.05,0.05,0.3])}{cylinder}
}


% Variables
def ax 30
def ay 35
def R 5

% Compute position of transponder
def T (R*sin(ax),R*sin(ay),0)   
% Compute transducer position
def P (0,0,sqrt(R^2-((T)'x^2+(T)'y^2)))  

def T_x ((T)'x,0)
def T_y (0,(T)'y)

def SSBLScene {
        
    {axes}
    
    % Draw projection of R
    line [line style=dashed,draw=gray] (T_x)(T)
    line [line style=dashed,draw=gray] (T_y)(T)
    line [line style=dashed,draw=gray] (T_y)(P)
    line [line style=dashed,draw=gray] (T_x)(P)
    line [draw=red] (P)(T)
    % draw labels
    special |\node[below right] at #1 {Transponder};|(T)  
    %dots(P)(T)
    special |\node[right] at #1 {$\mathrm{Transducer}$};|(P)  
    
    def dT unit((T)-(P))
    def dT_y unit((T_y) -(P))
    def dT_x unit((T_x) -(P))
    
    % Draw angle annotations
    def pp (P)+[dT]*1.5
    def N [dT]*[dT_y]
    def m rotate(ax/2,(P),[dT]*[dT_y])
    def n [dT] then rotate(ax/2,(P),[dT]*[dT_y])
    def nb [dT] then rotate(ax,(P),[dT]*[dT_y])
    def ppm (P)+[n]*1.5
    def ppb (P)+[nb]*1.5
    
    special |\draw #1..controls #2 .. node[near end,below] {$\theta$} #3;\;|(pp)(ppm)(ppb)
    % \uput[-60]#2{$\theta$}|(pp)(ppm)(ppb)
    def nc [dT] then rotate(ay/2,(P),[dT]*[dT_x])
    def nbc [dT] then rotate(ay,(P),[dT]*[dT_x])
    def ppmc (P)+[nc]*1.5
    def ppbc (P)+[nbc]*1.5
    special |\draw #1.. controls #2.. #3;\node[below] at #2 {$\phi$};|(pp)(ppmc)(ppbc)
    def mT (P)+((T)-(P))/2
    %special |\uput[dr]#1{$r$}|(mT)  
    
    put {translate((T)-(O))} {beacon}
    put {translate((P)-(O))}{put {scale(0.1)}{sphere}}
    
}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Define how to look at our work of art
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%def viewpoint (-4,14,-10)
def viewpoint (15,10,5)
def lookat (0, 0, 0)
% Set upvector along the z-axis
def upvector [0,0,1]



put{ view((viewpoint),(lookat), [upvector])} {
    %{axes}
    {SSBLScene}
    
    %
    %put {scale(1)}{fin}
}


global { language tikz }

